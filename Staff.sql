create database Esms;
use Esms;

create table `staff`(
    id                     int auto_increment comment 'id'  primary key,
    name                   varchar(512) null  comment '姓名',
    sex                    varchar(512) null  comment '性别',
    age                    int          null  comment '年龄',
    unit                   varchar(255) null  comment '单位',
    professional           varchar(512) null  comment '职位',
    basicSalary           int          null  comment '基本工资',
    welfare                int          null  comment '福利工资',
    reward                 int          null  comment '奖金',
    unemploymentInsurance int          null  comment '失业保险',
    housingProvidentFund int          null  comment '住房公积金',
    department             varchar(512) null  comment '科室'
);
insert into `staff` values(1,'gly','女',20,'1单位','经理',2000,500,500,100,200,'经理室');
insert into `staff` values(2,'gy','女',30,'','经理',1500,200,100,100,200,'财务室');
insert into `staff` values(3,'ljc','女',21,'','经理',1800,500,500,100,200,'技术科');
insert into `staff` values(4,'zzx','男',27,'','经理',1800,500,500,100,200,'技术科');
insert into `staff` values(5,'hj','女',29,'','经理',1000,100,200,100,200,'销售科');