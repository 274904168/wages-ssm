<%--
  Created by IntelliJ IDEA.
  User: demo
  Date: 2021/6/24
  Time: 3:47 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div style="width: 100%;height: 100px;display: flex;align-items: center;justify-content: center;text-align: center;">
        <h1>员工薪资管理系统</h1>
    </div>

    <form action="${pageContext.request.contextPath}/staff/addStaff" method="post">
        <div class="card">
            <div class="card-header">

                <h4 class="card-title">录入员工</h4>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>名称</label>
                    <input type="text" class="form-control" name="name" placeholder="请输入名称">
                </div>
                <div class="form-group">
                    <label>性别
                        <label class="radio-inline">
                            <input type="radio" name="sex" id="inlineRadio1" value="男"> 男
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="sex" id="inlineRadio2" value="女"> 女
                        </label>
                    </label>
                </div>

                <div class="form-group">
                    <label>年龄</label>
                    <input type="number" name="age" class="form-control" placeholder="请输入年龄">
                </div>
                <div class="form-group">
                    <label>单位</label>
                    <input type="text" name="unit" class="form-control" placeholder="请输入单位">
                </div>
                <div class="form-group">
                    <label>职业</label>
                    <input type="text" name="professional" class="form-control" placeholder="请输入职业">
                </div>
                <div class="form-group">
                    <label>基本工资</label>
                    <input type="number" name="basicSalary" class="form-control" placeholder="请输入基本工资">
                </div>
                <div class="form-group">
                    <label>福利工资</label>
                    <input type="number" name="welfare" class="form-control" placeholder="请输入福利工资">
                </div>
                <div class="form-group">
                    <label>奖励工资</label>
                    <input type="number" name="reward" class="form-control" placeholder="请输入奖励工资">
                </div>
                <div class="form-group">
                    <label>失业保险</label>
                    <input type="number" name="unemploymentInsurance" class="form-control"
                           placeholder="请输入失业保险">
                </div>
                <div class="form-group">
                    <label>住房公积金</label>
                    <input type="number" name="housingProvidentFund" class="form-control"
                           placeholder="请输入住房公积金">
                </div>
                <div class="form-group">
                    <label>科室</label>
                    <select class="form-control" name="department">
                        <option>经理室</option>
                        <option>财务科</option>
                        <option>技术科</option>
                        <option>销售科</option>
                    </select>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">录入</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
