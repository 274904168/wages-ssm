<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>员工管理展示</title>
    <%--BootStrap 美化界面--%>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    <small>员工管理列表 ------ 显示所有员工</small>
                </h1>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div style="width: 100%;height: 100px;display: flex;justify-content: flex-end;padding: 0 100px">

            <form action="${pageContext.request.contextPath}/staff/allStaff" method="get" style="display: flex">
                <select class="form-control" style="width: 90px;height: 40px;margin-right: 20px;" name="search">
                    <option></option>
                    <option>经理室</option>
                    <option>财务科</option>
                    <option>技术科</option>
                    <option>销售科</option>
                </select>

                <button style="width: 80px;height: 40px;margin-right: 20px;" type="submit" class="btn btn-primary"
                >刷新
                </button>
            </form>

            <a href="${pageContext.request.contextPath}/staff/addUser" type="_blank" >录入员工</a>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>姓名</th>
                    <th>性别</th>
                    <th>年龄</th>
                    <th>单位</th>
                    <th>职业</th>
                    <th>基本工资</th>
                    <th>福利工资</th>
                    <th>奖励工资</th>
                    <th>失业保险</th>
                    <th>住房积金</th>
                    <th>科室</th>
                    <th>实际应发工资</th>
                    <th>操作</th>
                </tr>
                </thead>
                <%--书籍从数据库中查询出来的，从list中遍历出来--%>
                <tbody>
                    <c:forEach var="staff" items="${list}">
                        <tr>
                            <td>${staff.id}</td>
                            <td>${staff.name}</td>
                            <td>${staff.sex}</td>
                            <td>${staff.age}</td>
                            <td>${staff.unit}</td>
                            <td>${staff.professional}</td>
                            <td>${staff.basicSalary}</td>
                            <td>${staff.welfare}</td>
                            <td>${staff.reward}</td>
                            <td>${staff.unemploymentInsurance}</td>
                            <td>${staff.housingProvidentFund}</td>
                            <td>${staff.department}</td>
                            <td>${staff.amount}</td>
                            <td>
                                <form action="${pageContext.request.contextPath}/staff/delete" method="post">
                                    <input type="hidden" name="id" value="${staff.id}">
                                    <button type="submit" class="btn btn-danger btn-sm">删除</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
