import com.win.service.StaffService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        StaffService staffServiceImpl = (StaffService)context.getBean("StaffServiceImpl");
        staffServiceImpl.queryAllStaff().forEach(System.out::println);
    }

}
