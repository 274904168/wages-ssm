package com.win.dao;

import com.win.pojo.Staffs;

import java.util.List;

public interface StaffMapper {

    //入职
    int addStaff(Staffs staffs);

    //离职
    int deleteStaff(int id);

    //查询
    Staffs queryStaffById(int id);

    List<Staffs> queryAllStaff();

    Integer countBy(String department);

    List<Staffs> queryByDepartment(String department);
}
