package com.win.controller;

import com.win.pojo.Staffs;
import com.win.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/staff")
public class StaffController {
    //从controller调service层
    @Autowired
    @Qualifier("StaffServiceImpl")
    private StaffService staffService;    //service层如果有多个实现类，利用此注解指定具体需要注入进来的service层实现类

    //为spring进行set的DL（依赖注入）提供方法
    public void setStaffService(StaffService staffService) {
        this.staffService = staffService;
    }

    //查询所有的员工，并且返回到一个员工展示页面
    @RequestMapping("/allStaff")
    public String list(Model model, HttpServletRequest request ) {
        System.out.println("========================Controller层，查询所有用户=================");
        String search = request.getParameter("search");
        if (search == null || search.equals("")) {
            List<Staffs> list = staffService.queryAllStaff();

            model.addAttribute("list", list);
        } else {
           List<Staffs> list = staffService.queryBy(search);
           model.addAttribute("list",list);
        }
        return "allStaff";
    }

    @RequestMapping("/addStaff")
    public void save(Staffs staffs, HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("========================Controller层，保存用户=================");
        staffService.addStaff(staffs);
        response.sendRedirect(request.getContextPath() + "/staff/allStaff");
    }

    @RequestMapping("/addUser")
    public String addUser() {
        return "addStaff";
    }

    @RequestMapping("/delete")
    public String delete(Integer id, HttpServletRequest request, HttpServletResponse response) {
        staffService.deleteStaff(id);
        return "allStaff";
    }

    @RequestMapping("/tongji")
    public String tongji(Model model) {
        model.addAttribute("list1", staffService.count());
        return "tongji";
    }
}
