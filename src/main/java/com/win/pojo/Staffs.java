package com.win.pojo;

public class Staffs {

	private int id;                     //id
	private String name;                //姓名
	private String sex;                 //性别
	private int age;                    //年龄
	private String unit;                //单位
	private String professional;        //职业
	private int basicSalary;            //基本工资
	private int welfare;                //福利工资
	private int reward;                 //奖励工资
	private int unemploymentInsurance;  //失业保险
	private int housingProvidentFund;   //住房基金
	private String department;          //科室
	private int amount;                 //实际应发工资

	public Staffs() {
	}

	public Staffs(int id, String name, String sex, int age, String unit, String professional, int basicSalary, int welfare, int reward, int unemploymentInsurance, int housingProvidentFund, String department, int amount) {
		this.id = id;
		this.name = name;
		this.sex = sex;
		this.age = age;
		this.unit = unit;
		this.professional = professional;
		this.basicSalary = basicSalary;
		this.welfare = welfare;
		this.reward = reward;
		this.unemploymentInsurance = unemploymentInsurance;
		this.housingProvidentFund = housingProvidentFund;
		this.department = department;
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getProfessional() {
		return professional;
	}

	public void setProfessional(String professional) {
		this.professional = professional;
	}

	public int getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(int basicSalary) {
		this.basicSalary = basicSalary;
	}

	public int getWelfare() {
		return welfare;
	}

	public void setWelfare(int welfare) {
		this.welfare = welfare;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getUnemploymentInsurance() {
		return unemploymentInsurance;
	}

	public void setUnemploymentInsurance(int unemploymentInsurance) {
		this.unemploymentInsurance = unemploymentInsurance;
	}

	public int getHousingProvidentFund() {
		return housingProvidentFund;
	}

	public void setHousingProvidentFund(int housingProvidentFund) {
		this.housingProvidentFund = housingProvidentFund;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Staff{" +
				"id=" + id +
				", name='" + name + '\'' +
				", sex='" + sex + '\'' +
				", age='" + age + '\'' +
				", unit='" + unit + '\'' +
				", professional='" + professional + '\'' +
				", basicSalary=" + basicSalary +
				", welfare=" + welfare +
				", reward=" + reward +
				", unemploymentInsurance=" + unemploymentInsurance +
				", housingProvidentFund=" + housingProvidentFund +
				", department='" + department + '\'' +
				", amount=" + (basicSalary+welfare+reward-unemploymentInsurance-housingProvidentFund) +
				'}';
	}
}
