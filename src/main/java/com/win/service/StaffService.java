package com.win.service;

import com.win.pojo.Staffs;

import java.util.List;

public interface StaffService {

    //入职
    int addStaff(Staffs staffs);

    //离职
    int deleteStaff(int id);

    //查询
    Staffs queryStaffById(int id);
    List<Staffs> queryAllStaff();

    List<Integer> count();

    List<Staffs> queryBy(String search);
}
