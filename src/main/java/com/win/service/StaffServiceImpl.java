package com.win.service;

import com.win.dao.StaffMapper;
import com.win.pojo.Staffs;

import java.util.ArrayList;
import java.util.List;

public class StaffServiceImpl implements StaffService{

    //service调dao层：组合dao
    private StaffMapper staffMapper;

    public void setStaffMapper(StaffMapper staffMapper) {
        this.staffMapper = staffMapper;
    }

    @Override
    public int addStaff(Staffs staffs) {
        return staffMapper.addStaff(staffs);
    }

    @Override
    public int deleteStaff(int id) {
        return staffMapper.deleteStaff(id);
    }

    @Override
    public Staffs queryStaffById(int id) {
        return staffMapper.queryStaffById(id);
    }

    @Override
    public List<Staffs> queryAllStaff() {
        return staffMapper.queryAllStaff();
    }

    @Override
    public List<Integer> count() {
        List<Integer> list = new ArrayList<>();
        list.add(staffMapper.countBy("经理室"));

        list.add(staffMapper.countBy("财务科"));
        list.add(staffMapper.countBy("技术科"));
        list.add(staffMapper.countBy("销售科"));
        return list;
    }

    @Override
    public List<Staffs> queryBy(String search) {
        return staffMapper.queryByDepartment(search);
    }
}
